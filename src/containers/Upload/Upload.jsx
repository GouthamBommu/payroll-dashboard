import React, { useState, useEffect, useRef } from "react";
import { Button, FormGroup, Col } from "reactstrap";
import { useMutation } from "@apollo/react-hooks";
import { NotificationManager } from "react-notifications";
import "react-notifications/lib/notifications.css";
import gql from "graphql-tag";

const UPLOAD_FILE = gql`
  mutation UPLOAD_FILE($file: Upload!) {
    uploadFile(file: $file)
  }
`;

const Upload = () => {
  const formElement = useRef();
  const [inputFile, setInputFile] = useState();
  const [
    uploadFile,
    { data: uploadFileResponse, error: uploadFileError },
  ] = useMutation(UPLOAD_FILE);

  useEffect(() => {
    if (uploadFileResponse) {
      NotificationManager.success("File uploaded successfully", "Upload");
      formElement.current.reset();
      setInputFile("");
    }
    if (uploadFileError) {
      NotificationManager.error("Upload failed", "Upload");
      formElement.current.reset();
      setInputFile("");
    }
  }, [uploadFileResponse, uploadFileError]);

  const onChangeFile = (event) => {
    const file = event.target.files[0];
    setInputFile(file);
  };

  const onHandleUpload = () => {
    uploadFile({
      variables: {
        file: inputFile,
      },
    });
  };

  return (
    <form id="formElement" ref={formElement} encType={"multipart/form-data"}>
      <FormGroup
        row
        style={{
          backgroundColor: "#615d5d",
          color: "white",
          alignItems: "center",
          justifyContent: "center",
          padding: "10px",
        }}
      >
        <Col style={{ padding: "10px" }} sm={2}>
          TIME REPORT :
        </Col>
        <Col style={{ padding: "10px" }} sm={4}>
          <div>
            <input
              type="file"
              accept=".csv"
              name="report"
              id="report"
              onChange={onChangeFile}
              style={{ borderStyle: "solid" }}
            />
          </div>
        </Col>
        <Col style={{ padding: "10px" }} sm={{ size: 1 }}>
          <Button
            onClick={onHandleUpload}
            disabled={!inputFile}
            color="primary"
          >
            Upload
          </Button>
        </Col>
      </FormGroup>
    </form>
  );
};

export default Upload;
