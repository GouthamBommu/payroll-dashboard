import React from "react";
import Layout from "../Layout/index";
import Upload from "../Upload/Upload";
import Payroll from "../Payroll/Payroll";
import { ApolloClient } from "apollo-client";
import { ApolloProvider } from "@apollo/react-hooks";
import { InMemoryCache } from "apollo-cache-inmemory";
import { createUploadLink } from "apollo-upload-client";
import { NotificationContainer } from "react-notifications";
import { Container, Row, Col } from "reactstrap";

const apolloCache = new InMemoryCache();

const link = createUploadLink({
  uri: "http://localhost:3500/", // Apollo Server is served from port 3500
});

const client = new ApolloClient({
  cache: apolloCache,
  link: link,
});

function App() {
  return (
    <ApolloProvider client={client}>
      <NotificationContainer />
      <Layout />
      <Upload />
      <Container fluid={true}>
        <Payroll />
      </Container>
    </ApolloProvider>
  );
}

export default App;
