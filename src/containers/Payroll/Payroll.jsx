import React, { useState, useEffect } from "react";
import { useQuery, useMutation } from "@apollo/react-hooks";
import MaterialTable from "material-table";
import gql from "graphql-tag";

const Payroll = () => {
  const GET_REPORT = gql`
    query GET_PAYROLL_REPORT {
      getPayrollReport {
        payrollReport {
          employeeReports {
            employeeId
            payPeriod {
              startDate
              endDate
            }
            amountPaid
          }
        }
      }
    }
  `;
  const columns = [
    {
      title: "Employee id",
      field: "employeeId",
      editable: "never",
    },
    {
      title: "Start date",
      field: "startDate",
      editable: "never",
    },
    { title: "End date", field: "endDate", editable: "never" },
    { title: "Amount paid", field: "amountPaid", editable: "never" },
  ];

  const [payrolls, setPayrolls] = useState([]);
  const { data, loading, error, refetch } = useQuery(GET_REPORT, {
    fetchPolicy: "network-only",
  });

  useEffect(() => {
    if (data) {
      const payrollReport = data.getPayrollReport.payrollReport;
      const employeeReports = payrollReport.employeeReports;
      let modifiedEmployeeReports = employeeReports.map((employeeReport) => {
        let modifiedEmployeeReport = {};
        modifiedEmployeeReport["employeeId"] = employeeReport["employeeId"];
        modifiedEmployeeReport["startDate"] =
          employeeReport.payPeriod["startDate"];
        modifiedEmployeeReport["endDate"] = employeeReport.payPeriod["endDate"];
        modifiedEmployeeReport["amountPaid"] = employeeReport["amountPaid"];

        return modifiedEmployeeReport;
      });
      setPayrolls(modifiedEmployeeReports);
    }
  }, [data]);

  if (loading) return "Loading";
  if (error) return "error";

  return (
    <MaterialTable
      title="Payrolls"
      columns={columns}
      data={payrolls}
    ></MaterialTable>
  );
};

export default Payroll;
